#!/bin/bash

# Поднимаем контейнеры
echo "Поднимаем контейнеры..."
docker run --name centos7 -d pycontribs/centos:7 sleep 36000000
docker run --name ubuntu -d pycontribs/ubuntu sleep 65000000
docker run --name fedora -d pycontribs/fedora sleep 36000000
# Ждем некоторое время, чтобы контейнеры успели запуститься
sleep 5

# Запускаем playbook с помощью Ansible
echo "Запускаем playbook..."
ansible-playbook --ask-vault-pass -i inventory/prod.yml site.yml

# Останавливаем контейнеры
echo "Останавливаем контейнеры..."
docker stop centos7 ubuntu fedora

# Удаляем контейнеры
echo "Удаляем контейнеры..."
docker rm centos7 ubuntu fedora

echo "Готово!"
